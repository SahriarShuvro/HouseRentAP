const inputFile = document.getElementById("tags");
const imgPreviewContainer = document.getElementById("imgPreviewContainer");

inputFile.addEventListener("change", function () {
  const images = this.files;

  // Clear existing previews
  imgPreviewContainer.innerHTML = "";

  for (let i = 0; i < images.length; i++) {
    const image = images[i];
    const reader = new FileReader();

    reader.onload = () => {
      const imgUrl = reader.result;
      const img = document.createElement("img");
      img.src = imgUrl;
      img.classList.add("imgPreview", "imageCard", "m-4");
      imgPreviewContainer.appendChild(img);
    };

    reader.readAsDataURL(image);
  }
});
